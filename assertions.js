/* jshint node:true, esnext: true */
/* globals describe, before, after, it */

'use strict';

require('co-mocha');

var assert = require('assert');

module.exports = function(name, db) {
	assert(describe && before && after && it, 'Missing mocha test runner');

	function assertions(collection) {
		// cleanup
		before(function *() {
			yield collection.flush({});
		});

		after(function *() {
			yield collection.flush({});
		});

		it('should do stuff :)', function *() {
			// insert user
			var user = yield collection.insert({ a: 1 });
			assert(user._id, 'Missing user id in inserted doc');

			// find user
			var foundUser = yield collection.findById(user._id);
			assert.deepEqual(foundUser, user, 'User mismatch');

			// update user
			var updated = yield collection.updateById(user._id, { a: 2 });
			assert.equal(updated, 1, 'User not updated');
			var updatedUser = yield collection.findById(user._id);
			assert.equal(updatedUser.a, 2, 'User not updated');

			// remove user
			var removed = yield collection.removeById(user._id);
			assert.equal(removed, 1, 'User not removed');
			var removedUser = yield collection.findById(user._id);
			assert.ok(!removedUser, 'User not removed');		
		});
	}

	describe('#gobind-db-' + name, function() {
		describe('#user', assertions(db.user));
	});
};