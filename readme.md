# Gobind db tests

Tests for gobind database adapters


## Usage

- Step 1 - Install required dependencies

```bash
npm install -g mocha # test runner

npm install --save-dev gobind-db-tests # test assertions
```


- Step 2 - Create test file

```js
// test.js

// change to actual location of file
var db = require('./index.js');

require('gobind-db-tests')('name', db);
```

- Step 3 - Add to package.json

```json
{
	...

	"scripts": {
		"test": "mocha --bail --harmony --reporter spec test.js"
	}

	...
}
```

- Step 4 - Run tests

```bash
npm test
```